# Unofficial(!!) Golang Trovo API Client

A dummy client to access the trovo api.

See <https://trovo.live>.

Docs for Trovo API under <https://trovo.live/policy/apis-developer-doc.html>.

This is a library, and not a fully working application! 
